# Computer Networks Wiki

[![pipeline status](https://gitlab.com/jarkomfasilkomui/2021-even/compnetwiki/badges/master/pipeline.svg)](https://gitlab.com/jarkomfasilkomui/2021-even/compnetwiki/-/commits/master)

This is a collaborative wiki that is authored by students of the Computer Networks course in Fasilkom UI for the 2020/2021 Even semester. This wiki provides an overview towards computer networking and is the first assignment for the course.

## Gain Edit Access (for students)

Computer Networks students who is currently working on this assignment can apply for edit access through the courses Discord channel (the link is provided in SCeLE). Please wait some time for the teaching team to invite you to this repository. If you have not received the invitation after 1-2 days, please notify the teaching team through Discord.

## Wiki Table of Contents
- What is the Internet?:
    - [What is the Internet?](a-what-is-the-internet/a1.md) 
    - [Communication Links and Packet Switching](a-what-is-the-internet/a2.md)
    - [Services](a-what-is-the-internet/a3.md)
    - [Protocols](a-what-is-the-internet/f3.md)
- Edge Networks:
    - [What is Edge Networks?](b-edge-networks/b1.md)
    - [Edge Network to ISP Connection](b-edge-networks/b2.md)
    - [Physical Media](b-edge-networks/b3.md)
    - [Local Network Topology](b-edge-networks/g3.md)
- Core Networks:
    - [What is Core Networks?](c-core-networks/c1.md)
    - [ISP Types](c-core-networks/c2.md)
    - [IXP and Content Provider Network](c-core-networks/c3.md)
    - [Circuit Switch and Packet Switch Networks](c-core-networks/c4.md)
- Delay, Packet Loss, and Throughput:
    - Delay:
        - [Processing and Queuing Delay](d-delay-packet-loss-throughput/d1.md)
        - [Transmission and Propagation Delay](d-delay-packet-loss-throughput/d2.md)
    - [Packet Loss](d-delay-packet-loss-throughput/d3.md)
    - [Throughput](d-delay-packet-loss-throughput/d4.md)
- Protocol Layers:
    - [Why Layering Model?](e-protocol-layers/e1.md)
    - [OSI Layering Mode](e-protocol-layers/e2.md)
    - [TCP/IP Layering Model](e-protocol-layers/e3.md)
- Network Security:
    - [Security Threats](f-network-security/f1.md)
    - [Security Attacks](f-network-security/f2.md)
- History of Internet:
    - [World](g-history-of-internet/g1.md)
    - [Indonesia](g-history-of-internet/g2.md)

## License

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Copyright (C) 2021, Faculty of Computer Science Universitas Indonesia

The works in this repository is licensed under the Creative-Commons Attribution-NonCommercial-ShareAlike 4.0 International license.