# Computer Networks Wiki

[![pipeline status](https://gitlab.com/jarkomfasilkomui/2021-even/compnetwiki/badges/master/pipeline.svg)](https://gitlab.com/jarkomfasilkomui/2021-even/compnetwiki/-/commits/master)

This is a collaborative wiki that is authored by students of the Computer Networks course in Fasilkom UI for the 2020/2021 Even semester. This wiki provides an overview towards computer networking and is the first assignment for the course.

## Gain Edit Access (for students)

Computer Networks students who is currently working on this assignment can apply for edit access through the course's Discord channel (the link is provided in SCeLE). Please wait some time for the teaching team to invite you to this repository. If you have not received the invitation after 1-2 days, please notify the teaching team through Discord.

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):

```
image: python:buster

before_script:
  - pip install -r requirements.txt

test:
  stage: test
  script:
  - mkdocs build --strict --verbose --site-dir test
  artifacts:
    paths:
    - test
  except:
  - master

pages:
  stage: deploy
  script:
  - mkdocs build --strict --verbose
  artifacts:
    paths:
    - public
  only:
  - master
```

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)

Read more at MkDocs [documentation][].



## Troubleshooting

1. CSS is missing! That means two things:

    Either that you have wrongly set up the CSS URL in your templates, or
    your static generator has a configuration option that needs to be explicitly
    set in order to serve static assets under a relative URL.

[ci]: https://about.gitlab.com/gitlab-ci/
[mkdocs]: http://www.mkdocs.org
[install]: http://www.mkdocs.org/#installation
[documentation]: http://www.mkdocs.org
[userpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#user-or-group-pages
[projpages]: https://docs.gitlab.com/ce/user/project/pages/introduction.html#project-pages

---

Forked from https://gitlab.com/morph027/mkdocs

## License

[![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Copyright (C) 2021, Faculty of Computer Science Universitas Indonesia

The works in this repository is licensed under the Creative-Commons Attribution-NonCommercial-ShareAlike 4.0 International license.
